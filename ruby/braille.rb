#!/bin/ruby

$braille_map = {
  "100000" => "a",
  "110000" => "b",
  "100100" => "c",
  "100110" => "d",
  "100010" => "e",
  "110100" => "f",
  "110110" => "g",
  "110010" => "h",
  "010100" => "i",
  "010110" => "j",
  "101000" => "k",
  "111000" => "l",
  "101100" => "m",
  "101110" => "n",
  "101010" => "o",
  "111100" => "p",
  "111110" => "q",
  "111010" => "r",
  "011100" => "s",
  "011110" => "t",
  "101001" => "u",
  "111001" => "v",
  "010111" => "w",
  "101101" => "x",
  "101111" => "y",
  "101011" => "z",
}

$weird_number_map = {
  "001011" => 0,
  "010000" => 1,
  "011000" => 2,
  "010010" => 3,
  "010011" => 4,
  "010001" => 5,
  "011010" => 6,
  "011011" => 7,
  "011001" => 8,
  "001010" => 9,
}

def morse2bin(string)
  v1 = string.gsub(/[.-]/, "." => "0", "-" => "1")
  v2 = string.gsub(/[.-]/, "." => "1", "-" => "0")
  
  return v1, v2
end

def bin2braille(binary_str)
  braille_array = binary_str.scan(/.{1,6}/)
  braille_array.map! do |data| 
    if $braille_map.key? data
      $braille_map[data]
    elsif $weird_number_map.key? data
      $weird_number_map[data]
    else
      "?"
    end
  end

  return braille_array.join
end

def morse_check(str)
  return str !~ /[^\.\s-]/
end

# for alternate numberings...
def transpose(string)
  string_arr = string.scan(/.{1,6}/)
  string_arr.map! do |bin_data|
    bin_data[0] + bin_data[2] + bin_data[4] + bin_data[1] + bin_data[3] + bin_data[5]
  end

  return string_arr.join
end

puts "The script will turn a morse string (with or without spaces) into a binary stream of data, and attempt to convert to morse"
puts 
puts "It will try both '.' = 0 and '.' = 1, as well as both enumerations of the braille dots"
puts
puts "Please enter your morse string..."

morse_str = gets.chomp
until morse_check(morse_str)
  puts "Not a valid morse string, please try again, or press 'q' and enter to exit..."
  morse_str = gets.chomp
  if morse_str == "q"
    puts "Exiting..."
    exit
  end
end

morse_str = morse_str.gsub(" ", "")

unless morse_str.length % 6 == 0
  puts "Not divisible by 6, does not fit braille"
  exit
end

bin_arr = morse2bin(morse_str)

bin_arr = bin_arr + bin_arr.map{ |bin_str| transpose(bin_str)}

braille_arr = bin_arr.map{ |bin_str| bin2braille(bin_str)}

puts "Here are your possible braille mappings, where '?' indicates not an english character (check for NIA weirdness)..."

puts braille_arr
