#!/bin/ruby

$morse_dict = {
  "a" => ".-",
  "b" => "-...",
  "c" => "-.-.",
  "d" => "-..",
  "e" => ".",
  "f" => "..-.",
  "g" => "--.",
  "h" => "....",
  "i" => "..",
  "j" => ".---",
  "k" => "-.-",
  "l" => ".-..",
  "m" => "--",
  "n" => "-.",
  "o" => "---",
  "p" => ".--.",
  "q" => "--.-",
  "r" => ".-.",
  "s" => "...",
  "t" => "-",
  "u" => "..-",
  "v" => "...-",
  "w" => ".--",
  "x" => "-..-",
  "y" => "-.--",
  "z" => "--..",
  "1" => ".----",
  "2" => "..---",
  "3" => "...--",
  "4" => "....-",
  "5" => ".....",
  "6" => "-....",
  "7" => "--...",
  "8" => "---..",
  "9" => "----.",
  "0" => "-----",
}
$rev_dict = $morse_dict.invert

def morse_flip(morse_str)
  morse_str.gsub(/[.-]/, "." => "-", "-" => ".")
end

def morse_reverse(morse_str)
  morse_str.reverse
end

def alpha_check(str)
  return str !~ /[^a-zA-Z0-9]/
end

def check_for_alphanumeric(enc_str)
  until alpha_check(enc_str)
    puts "Not an alphanumeric string, please try again, or press 'q' and enter to exit..."
    enc_str = gets.chomp
    if enc_str == "q"
      puts "Exiting..."
      exit
    end
  end
end

def alpha2morse(str)
  array = str.split ""
  array.map!{ |char| $morse_dict[char]}

  return array.join " "
end

def morse2alpha(str)
  array = str.split " "
  array.map! do |morse_char|
    if $rev_dict.key? morse_char
      $rev_dict[morse_char]
    else
      "?"
    end
  end

  return array.join
end
  

puts "This will take in an alphanumeric string (no punctuation), convert to morse, and spit out the morse string."
puts "If you like, you can also see the reversed, flipped, and reversed and flipped strings"
puts "Enter alphanumeric string..."
enc_str = gets.chomp

check_for_alphanumeric(enc_str)

morse_str = alpha2morse(enc_str)
puts "Here's your string in morse!"
puts morse_str

puts "Do you want the morse transforms mentioned earlier? (y/n)"
answer = gets.chomp
until answer == "y" || answer == "n"
  puts "Please say y or n"
  answer = gets.chomp
end

if answer == "n"
  puts "Goodbye!"
  exit
end

puts "Here is the morse string reversed and back to alphanumeric..."
puts morse2alpha(morse_reverse(morse_str))
puts
puts "Here is the morse string with a bit flip"
puts morse2alpha(morse_flip(morse_str))
puts
puts "Here is bit flip and reversed"
puts morse2alpha(morse_reverse(morse_flip(morse_str)))
  