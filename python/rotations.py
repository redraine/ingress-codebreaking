#!/bin/python3

import keywords

def alpha_shift(char, rot):
  return chr(((ord(char) - 97 + rot) % 26) + 97);

def num_shift(num, rot):
  return str((int(num) + rot) % 10);

def caesarian_rotation(string):
    valid_dict = {}
    for rot in range(0,26):
        new_str = ""
        for char in string:
            if char.isdigit():
                new_str += num_shift(char,rot)
            elif char.isalpha():
                new_str += alpha_shift(char,rot)

        valid_keywords = keywords.keyword_existence(new_str,True)
      
        if valid_keywords:
            valid_dict[rot] = [valid_keywords, new_str]
        else:
            continue
        
    return valid_dict;

def main(encrypted_string):
    valid_dict = caesarian_rotation(encrypted_string)
    if len(valid_dict) == 0:
        print("No keywords found in any rot")
    else:
        for k in valid_dict:
            print(f'Rot {k} resulted in possible keyword(s) {valid_dict[k][0]} and rotated string {valid_dict[k][1]}')
    
    return;