#!/bin/python3

import keywords

def alpha_atbash(char):
    return chr((ord("z") - ord(char) + 97));
    
def num_atbash(char, flag=0):
    if flag == 0:
        return str((10 - int(char)) % 10);
    elif flag == 1:
        return str((11 - int(char)) % 10);
    else:
        return char;
    
def atbash_string(string):
    new_strings = []
    for flag in range(0,3):
        new_string = ""
        for char in string:
            if char.isdigit():
                new_string += num_atbash(char,flag)
            elif char.isalpha():
                new_string += alpha_atbash(char)
        new_strings.append(new_string)
            
    return new_strings;

def main(encrypted_string):
    atbash_strings = atbash_string(encrypted_string)
    test_string = atbash_strings[0]
    no_numbers = encrypted_string.isalpha()

    plain_keywords = keywords.keyword_existence(test_string)
    atbash_map = keywords.atbash_maps_existence(test_string)

    print ("Here are the atbash string(s)...")
    if no_numbers:
        print(atbash_strings[0])
    else:
        print ("1 = 9")
        print (atbash_strings[0])
        print ("1 = 0")
        print (atbash_strings[1])
        print ("1 = 1")
        print (atbash_strings[2])

    if len(plain_keywords) == 0 and len(atbash_map) == 0:
        print ("No keywords of suitable length found for atbash.")
    else:
        print ("Here are the possible keyword/atbash mappings...")
        if len(atbash_map) == 0:
            print ("No known atbash mappings...")
        else:
            print ("Here are the possible atbash mappings found in the string...")
            for k in atbash_map:
                print (k + "->" + atbash_map[k])
                
        if len(plain_keywords) == 0:
            print ("No plaintext keywords...")
        else:
            print ("Here are the plaintext keywords found in the atbash string...")
            for keyword in plain_keywords:
                print (keyword)
                
    return;

        
    
    
    

    