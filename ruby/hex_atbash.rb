#!/bin/ruby

def hex_atbash(char)
  (0xf - char.to_i(16)).to_s(16)
end

def hex_check(str)
  return str !~ /[^a-f0-9]/
end

def atbash(str)
  str.split("").map{ |char| hex_atbash(char)}.join
end

puts "Please enter hex string to atbash..."
hex_str = gets.chomp

unless hex_check(hex_str)
  puts "Not a valid hex string, exitting!"
  exit 1
end

atbash_str = atbash(hex_str)
puts "Hex atbash:"
puts atbash_str
puts "Directly to ascii..."
puts [atbash_str].pack('H*')
if atbash_str !~ /[^0-9]/
  puts "Interpreted as decimal..."
  puts atbash_str.scan(/.{1,2}/).map{ |pair| pair.to_i.chr }.join
end

  