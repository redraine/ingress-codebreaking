#!/bin/python3

import re
import keywords

braille_map = {  "100000":"a",  "110000":"b",  "100100":"c",  "100110":"d",  "100010":"e",  "110100":"f",  "110110":"g",  "110010":"h",  "010100":"i",  "010110":"j",  "101000":"k",  "111000":"l",  "101100":"m",  "101110":"n",  "101010":"o",  "111100":"p",  "111110":"q",  "111010":"r",  "011100":"s",  "011110":"t",  "101001":"u",  "111001":"v",  "010111":"w",  "101101":"x",  "101111":"y",  "101011":"z"}

weird_number_map = {  "001011":"0",  "010000":"1",  "011000":"2",  "010010":"3",  "010011":"4",  "010001":"5",  "011010":"6",  "011011":"7",  "011001":"8",  "001010":"9"}

def morse2bin(morse_str):
    stripped_str = re.sub(r'\s+', '', morse_str)
    v1 = stripped_str.translate(morse_str.maketrans(".-", "01"))
    v2 = stripped_str.translate(morse_str.maketrans(".-", "10"))
    return [v1,v2];

def bin2braille(bin_str):
    braille_array = re.findall('.{%d}' % 6, bin_str)
    new_str = ""
    
    for seq in braille_array:
        try:
            new_str += braille_map[seq]
        except KeyError:
            try:
                new_str += weird_number_map[seq]
            except KeyError:
                new_str += "?"

    return new_str;

# for alternate numberings...
def transpose(string):
    array = re.findall('.{%d}' % 6, string)
    transpose_str = ""
    for seq in array:
        transpose_str += seq[0] + seq[2] + seq[4] + seq[1] + seq[3] + seq[5]
    return transpose_str;

def main(morse_str):
    bin_strs = morse2bin(morse_str)
    bin_strs.append(transpose(bin_strs[0]))
    bin_strs.append(transpose(bin_strs[1]))
    
    plain_strs = []
    for bin_str in bin_strs:
        plain_strs.append(bin2braille(bin_str))

    print("Here are the possible braille strings...")
    for plain_str in plain_strs:
        print(plain_str)
        keywords.keywords_detected(plain_str)
        print()

    return;