#!/usr/bin/ruby
require "json"

if File.exists?("keywords.json")
  keyword_arr = JSON.parse(File.read("keywords.json"))["keywords"]
  $keyword_possibilities = []
  keyword_arr.each do |keyword_hash_bit|
    $keyword_possibilities << keyword_hash_bit["word"]
    unless keyword_hash_bit["atbash"].nil?
      $keyword_possibilities << keyword_hash_bit["atbash"]
    end
  end
  $keyword_possibilities = $keyword_possibilities.flatten.uniq.sort
else
  puts "Keywords json missing! Please put a JSON file with known keywords named 'keyword.json' in the directory"
  puts "Exiting..."
  exit 1
end

def alpha_shift(char, rot)
  temp = char.ord - 97 + rot 
  temp = (temp % 26) + 97
  return temp.chr
end

def num_shift(num, rot)
  (num.to_i + rot) % 10
end

def keyword_check(str)
  valid_arr = []
  $keyword_possibilities.each do |keyword|
    if str.include? keyword
      valid_arr << keyword
    end
  end
  return valid_arr
end

def caesarian_shifts(string)
  char_arr = string.split("")
  valid_hash = {}
  (0..25).each do |rot|
    test_str = char_arr.map do |char|
      if char =~ /[a-z]/
        alpha_shift(char,rot)
      elsif char =~ /[0-9]/
        num_shift(char,rot)
      end
    end
    test_str = test_str.join
    valids = keyword_check(test_str)
    valids = valids.reject { |word| word.length < string.length - 15 }
    next if valids.empty?
    valid_hash[rot.to_s] = valids, test_str
  end
  return valid_hash
end

def alpha_check(str)
  return str !~ /[^a-zA-Z0-9]/
end

def check_for_alphanumeric(enc_str)
  until alpha_check(enc_str)
    puts "Not an alphanumeric string, please try again, or press 'q' and enter to exit..."
    enc_str = gets.chomp
    if enc_str == "q"
      puts "Exiting..."
      exit
    end
  end
end

puts "Checking for keywords or atbashes in caesarian rotation..."
puts "Enter string..."
enc_str = gets.chomp

check_for_alphanumeric(enc_str)

enc_str = enc_str.downcase

possible_hash = caesarian_shifts(enc_str)
puts "Caesarian possibilities:"
puts possible_hash
