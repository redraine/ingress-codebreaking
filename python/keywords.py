#!/bin/python3

import sys
import json


def load_json():
    try:
        file = open("./keywords.json", "r")
    except IOError:
        print("'keywords.json' does not seem to exist, please download the latest from ingress-codes...")
        sys.exit()

    keyword_json_arr = json.load(file)['keywords']

    return keyword_json_arr;

def keyword_arr(include_atbash = False):

    keyword_json_arr = load_json()

    uniq_set = set()

    for keyword_hash in keyword_json_arr:
        uniq_set.add(keyword_hash["word"])
        # if not blank, we want to check passcode string for atbash candidates
        if keyword_hash["atbash"] and include_atbash:
            for atbash in keyword_hash["atbash"]:
                uniq_set.add(atbash)

    uniq_array = list(uniq_set)
    uniq_array.sort()

    return uniq_array;
    
def atbash_dict():
    
    keyword_json_arr = load_json()
    
    atbash_dict = dict()
    
    for keyword_hash in keyword_json_arr:
        # if blank, no known atbash candidates exist
        if keyword_hash["atbash"]:
            for atbash in keyword_hash["atbash"]:
                atbash_dict[atbash] = keyword_hash["word"]
    
    return atbash_dict;

def keyword_length_check(keyword, string):
    return len(keyword) < (len(string) - 15);

def keyword_existence(string, include_atbash=False):
    keyword_array = keyword_arr(include_atbash)

    possible_keywords = []

    for keyword in keyword_array:
        if keyword_length_check(keyword,string):
            next
        else:
            try:
                string.index(keyword)
                possible_keywords.append(keyword)
            except ValueError:
                continue

    return possible_keywords;

def atbash_maps_existence(string):
    atbash_map = atbash_dict()

    possible_atbashes = {}

    for atbash in atbash_map:
        if keyword_length_check(atbash,string):
            next
        else:
            try:
                string.index(atbash)
                possible_atbashes[atbash] = atbash_map[atbash]
            except ValueError:
                continue

    return possible_atbashes;

def keywords_detected(string):
    if keyword_existence(string):
        print("Following keywords detected:")
        for keyword in keyword_existence(string):
            print(keyword)
    else:
        print("No keywords detected")

    return;