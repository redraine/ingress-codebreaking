#!/bin/python3
import keywords

morse_dict = {  "a":".-",  "b":"-...",  "c":"-.-.",  "d":"-..",  "e":".",  "f":"..-.",  "g":"--.",  "h":"....",  "i":"..",  "j":".---",  "k":"-.-",  "l":".-..",  "m":"--",  "n":"-.",  "o":"---",  "p":".--.",  "q":"--.-",  "r":".-.",  "s":"...",  "t":"-",  "u":"..-",  "v":"...-",  "w":".--",  "x":"-..-",  "y":"-.--",  "z":"--..",  "1":".----",  "2":"..---",  "3":"...--",  "4":"....-",  "5":".....",  "6":"-....",  "7":"--...",  "8":"---..",  "9":"----.",  "0":"-----"}
alpha_dict = dict((morse_dict[k],k) for k in morse_dict)

def morse_flip(morse_str):
    return morse_str.translate(morse_str.maketrans(".-", "-."));

def morse_reverse(morse_str):
    return morse_str[::-1];

def alpha2morse(alpha_str):
    new_str = ""
    for char in alpha_str:
        try:
            new_str += morse_dict[char]
        except KeyError:
            new_str += "?"
        new_str += " "

    return new_str[0:-1];

def morse2alpha(morse_str):
    new_str = ""
    for group in morse_str.split(" "):
        try:
            new_str += alpha_dict[group]
        except:
            new_str += "?"
            
    return new_str;

def main(encrypted_string):
    print("Morse String:")
    morse_str = alpha2morse(encrypted_string)
    print(morse_str)
    print()
    print("Morse Flip:")
    flip_str = morse2alpha(morse_flip(morse_str))
    print(flip_str)
    keywords.keywords_detected(flip_str)
    print()
    print("Morse Reverse:")
    reverse_str = morse2alpha(morse_reverse(morse_str))
    print(reverse_str)
    keywords.keywords_detected(reverse_str)
    print()
    print("Morse Flip+Reverse:")
    flip_reverse = morse2alpha(morse_flip(morse_reverse(morse_str)))
    print(flip_reverse)
    keywords.keywords_detected(flip_reverse)
  
    return morse_str;