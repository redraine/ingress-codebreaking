#!/usr/bin/ruby
require "json"

if File.exists?("keywords.json")
  keyword_arr = JSON.parse(File.read("keywords.json"))["keywords"]
  $atbash_possibilities = {}
  keyword_arr.each do |keyword_hash_bit|
    next if keyword_hash_bit["atbash"].nil?
    keyword_hash_bit["atbash"].each do |atbash|
      unless $atbash_possibilities.key? atbash
        $atbash_possibilities[atbash] = [keyword_hash_bit["word"]]
      else
        $atbash_possibilities[atbash] << keyword_hash_bit["word"]
      end
    end
  end
  $keyword_possibilities = []
  keyword_arr.each do |keyword_hash_bit|
    $keyword_possibilities << keyword_hash_bit["word"]
    unless keyword_hash_bit["atbash"].nil?
      $keyword_possibilities << keyword_hash_bit["atbash"]
    end
  end
  $keyword_possibilities = $keyword_possibilities.flatten.uniq.sort
else
  puts "Keywords json missing! Please put a JSON file with known keywords named 'keyword.json' in the directory"
  puts "Exiting..."
  exit 1
end

def alpha_atbash(char)
  return ("z".ord - char.ord + 97).chr
end

def num_atbash(num, flag)
  case flag
  when 0
    (10 - num.to_i) % 10
  when 2
    (11 - num.to_i) % 10
  end
end

def keyword_check(str)
  valid_arr = []
  $keyword_possibilities.each do |keyword|
    if str.include? keyword
      valid_arr << keyword
    end
  end
  return valid_arr
end

def atbash_check(str)
  valid_hash = {}
  $atbash_possibilities.keys.each do |key|
    next unless str.include? key
    valid_hash[key] = $atbash_possibilities[key]
  end
  return valid_hash
end

def atbash(string, flag=0)
  char_arr = string.split("")
  char_arr.map! do |char|
    if char =~ /[a-z]/
      alpha_atbash(char)
    elsif char =~ /[0-9]/
      case flag
      when 0
        num_atbash(char, flag)
      when 1
        char
      when 2
        num_atbash(char,flag)
      end
    end
  end
  return char_str = char_arr.join
end

def alpha_check(str)
  return str !~ /[^a-zA-Z0-9]/
end

def check_for_alphanumeric(enc_str)
  until alpha_check(enc_str)
    puts "Not an alphanumeric string, please try again, or press 'q' and enter to exit..."
    enc_str = gets.chomp
    if enc_str == "q"
      puts "Exiting..."
      exit
    end
  end
end

puts "Checking for keywords or atbashes in atbash..."
puts "Enter string..."
enc_str = gets.chomp

check_for_alphanumeric(enc_str)

enc_str = enc_str.downcase

if enc_str =~ /[0-9]/
  puts "Atbash string (1 = 9):"
  puts atbash(enc_str)
  puts "Atbash string (1 = 1):"
  puts atbash(enc_str, 1)
  puts "Atbash string (1 = 0):"
  puts atbash(enc_str, 2)
else
  puts "Atbash string:"
  puts atbash(enc_str)
end
puts 
puts "(Standard) Keywords:"
puts keyword_check(atbash(enc_str))
puts 
puts "Keywords that can be subbed:"
puts atbash_check(atbash(enc_str))
