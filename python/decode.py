#!/bin/python3
import braille
import morse
import atbash
import rotations
import re

def main():
    passcode = input("Please enter a encrypted passcode:")
    
    downcase_passcode = passcode.lower()
    
    print("Trying bruteforce rotations...")
    rotations.main(downcase_passcode)
    
    print()
    print("Trying atbash...")
    atbash.main(downcase_passcode)

    print()
    print("Trying morse...")
    morse_str = morse.main(downcase_passcode)

    if len(re.sub(r'\s+', '', morse_str)) % 6 == 0:
        print("Morse string divisible by 6, trying braille...")
        braille.main(morse_str)
    
    return;
    
main()    